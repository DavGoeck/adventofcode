# Advent Of Code - Java Template

This project should quickly get you up and running with your AdventOfCode
challenge. 

## How does it work?

Imagine you want to solve the puzzle of day number *X*. 

1. Download the input for the puzzle and save it in *src/inputs* as *some-name.txt*.
2. Add a new class in the puzzles directory and extend *AdventPuzzle*\*.
3. Override `void firstPart()` and `void secondPart()`.
4. Register the puzzle in the `switch` expression in the *Solution.java* file, i.e. `X -> new MyNewlyCreatedPuzzle()` where *X* is the puzzles number.
5. Need any libraries? Download and drop them in *libs/* as a *.jar*.
6. *Compile*\**: `javac -cp src:libs/* -d out/ src/advent/Advent.java`
7. *Run day X*: `java -cp out:libs/* advent.Advent X`

 \*you can also just implement *Puzzle* but *AdventPuzzle* already provides some convenience for reading inputs and presenting results as well as measuring the runtime of your solution.

\**the project requires Java 17 as it contains `switch` expressions