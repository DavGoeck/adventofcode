package advent;

import advent.puzzles.Puzzle;

public class Advent {

	public static void main(String[] args) {

		Puzzle puzzle = determinePuzzle(args);
		puzzle.solve();
	}

	private static Puzzle determinePuzzle(String[] args) {
		int number = checkPuzzleNumber(args);
		return Solution.forNumber(number);
	}

	private static int checkPuzzleNumber(String[] args) {
		if (args.length != 1) throw new IllegalArgumentException();
		int number = Integer.parseInt(args[0]);
		if ( number <= 0 || 25 < number ) throw new IllegalArgumentException();
		return number;
	}
}
