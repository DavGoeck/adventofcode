package advent;

import advent.puzzles.*;

public class Solution {

	public static Puzzle forNumber(int n) {
		return switch(n) {
			// register new puzzles here
			default -> new NoopPuzzle();
		};
	}
}
