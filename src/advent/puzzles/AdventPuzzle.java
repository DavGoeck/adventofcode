package advent.puzzles;

import java.io.File;
import java.io.FileNotFoundException;

import java.time.Clock;
import java.time.Instant;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;

public class AdventPuzzle implements Puzzle {

	Clock clock = Clock.systemUTC();

	@Override
	public void solve() {
		runAndTimeFirstPart();
		runAndTimeSecondPart();
	}

	private void runAndTimeFirstPart() {
		runAndTime(this::firstPart);
	}

	private void runAndTimeSecondPart() {
		runAndTime(this::secondPart);
	}

	private void runAndTime(Runnable procedure) {
		Instant start = Instant.now(clock);
		procedure.run();
		Instant end = Instant.now(clock);
		long timeForFirst = (end.toEpochMilli() - start.toEpochMilli());
		System.out.println(" (Took %sms)\n".formatted(timeForFirst));	
	}

	protected void firstPart() {
		System.out.print("The first part has not been implemented yet!");
	}

	protected void secondPart() {
		System.out.print("The second part has not been implemented yet!");
	}

	protected static List<String> readInputs(String filename) {
		List<String> inputs = new ArrayList<>();

		readInputAndConsume(filename, inputs::add);

		return inputs;
	}

	protected static String readInputsAsLine(String filename) {
		StringBuilder sb = new StringBuilder();

		readInputAndConsume(filename, sb::append);

		return sb.toString();
	}

	private static void readInputAndConsume(String filename, Consumer<String> consumer) {
		try {
			Scanner sc = new Scanner(new File("src/inputs/%s.txt".formatted(filename)));
			while(sc.hasNextLine()) {
				String line = sc.nextLine();
				consumer.accept(line);
			}

		} catch (FileNotFoundException e) {
			throw new RuntimeException("File not found!");
		}
	}

	protected Integer asInt(String s) {
		return Integer.parseInt(s);
	}

	protected void presentResult(String message, Object... args) {
		System.out.print(message.formatted(args));
	}
}

