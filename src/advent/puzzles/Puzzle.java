package advent.puzzles;

public interface Puzzle {

	void solve();
}
